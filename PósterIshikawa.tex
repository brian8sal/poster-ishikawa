%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% a0poster Portrait Poster
% LaTeX Template
% Version 1.0 (22/06/13)
%
% The a0poster class was created by:
% Gerlinde Kettl and Matthias Weiser (tex@kettl.de)
% 
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[a0,portrait]{a0poster}

\usepackage{multicol} % This is so we can have multiple columns of text side-by-side
\columnsep=100pt % This is the amount of white space between the columns in the poster
\columnseprule=3pt % This is the thickness of the black line between the columns in the poster

\usepackage[svgnames]{xcolor} % Specify colors by their 'svgnames', for a full list of all colors available see here: http://www.latextemplates.com/svgnames-colors

\usepackage{times} % Use the times font
%\usepackage{palatino} % Uncomment to use the Palatino font

\usepackage{graphicx} % Required for including images
\graphicspath{{figures/}} % Location of the graphics files
\usepackage{booktabs} % Top and bottom rules for table
\usepackage[font=small,labelfont=bf]{caption} % Required for specifying captions to tables and figures
\usepackage{amsfonts, amsmath, amsthm, amssymb} % For math fonts, symbols and environments
\usepackage{wrapfig} % Allows wrapping text around tables and figures

\definecolor{tealblue}{rgb}{0.21, 0.46, 0.53}
\begin{document}

%----------------------------------------------------------------------------------------
%	POSTER HEADER 
%----------------------------------------------------------------------------------------

% The header is divided into two boxes:
% The first is 75% wide and houses the title, subtitle, names, university/organization and contact information
% The second is 25% wide and houses a logo for your university/organization or a photo of you
% The widths of these boxes can be easily edited to accommodate your content as you see fit

\begin{minipage}[b]{0.75\linewidth}
\veryHuge \color{NavyBlue}  \textbf{Domain-Specific Languages for the Automated Generation of Datasets for Industry 4.0} \color{Black}\\ % Title
\Huge\textit{}\\[2cm] % Subtitle
\huge \textbf{Brian Sal Sarria}\\[0.5cm] % Author(s)
\huge Universidad de Cantabria - Grupo de Ingeniería de Software y Tiempo Real\\[0.4cm] % University/organization
\Large \texttt{brian.sal@unican.es}\\
\end{minipage}
%
\begin{minipage}[b]{0.25\linewidth}
\includegraphics[width=17cm]{logo.png}\\
\end{minipage}

\vspace{0.5cm} % A bit of extra whitespace between the header and poster content


\begin{multicols}{2} % This is how many columns your poster will be broken into, a portrait poster is generally split into 2 columns

\color{tealblue} % SaddleBrown color for the introduction

\section*{Highlights}

\begin{itemize}
	\item Production lines of manufacturing industries are comprised of  interconnected elements that gather a wide range of data about how they work.
	\item  Industry 4.0 applications are often built on data analysis algorithms that aim to improve the system that they serve to.
	\item Most data analysis algorithms only accept as input data arranged in a very specific tabular format, known as dataset.
	\item Domain-Specific Languages might be a key element for the democratization of data analysis.
\end{itemize}

\color{DarkSlateGray} % DarkSlateGray color for the rest of the content

\section*{The Dataset Formatting Problem}

\begin{itemize}
	\item To satisfy the \textit{one entity, one row} constraint required by data analysis algorithms, data must undergo numerous transformations.
	\item Languages such as SQL and libraries such as Pandas, allow experts to develop scripts that prepare data to serve as input for data analysis algorithms. These scripts are labor intensive and prone to errors.
	\begin{center}\vspace{1cm}
		\includegraphics[width=0.9\linewidth]{formatting-problem}
		\captionof{figure}{\color{tealblue} The dataset formatting problem}
	\end{center}
\end{itemize}

\section*{Lavoisier}
\begin{itemize}
	\item \textit{Lavoisier} \cite{DelaVega2019,DelaVega2020} is a declarative language for data selection and formatting that provides a set of high-level primitives that allows focusing on what data must be selected and skips the details of how these data need to be transformed to be formatted as a dataset.
	\item Using \textit{Lavoisier}, complexity of data reshaping scripts decreases by ~60\%.
	\item \textit{Lavoisier} relies on object-oriented models to describe domain-data but industrial engineers are not used to deal with these models; but they are used to fishbone diagrams \cite{Ishikawa1976}.
	\begin{center}\vspace{1cm}
		\includegraphics[width=0.9\linewidth]{lavoisier}
		\captionof{figure}{\color{tealblue} An example of Lavoisier specification with the model it uses and the dataset it generates}
	\end{center}
\end{itemize}


%\section*{Fishbone Diagrams}
%
%\begin{itemize}
%	\item \textit{fishbone Diagrams}, also known as \textit{Cause-Effect}, \textit{Ishikawa} or \textit{Fishikawa
%	diagrams}, aim to identify causes that might lead to a certain effect
%\end{itemize}
%
%\begin{center}\vspace{1cm}
%	\includegraphics[width=0.9\linewidth]{falling-band-ishikawa}
%	\captionof{figure}{\color{Green} Example of an fishbone diagram}
%\end{center}

\section*{Solution Overview}

\begin{itemize}
	\item The solution proposed here seeks to adapt Lavoisier to work with fishbone diagrams.
	\item Three Domain-Specific Languages were designed and tools for its use developed: (1) \textit{Quality Control Fishbone diagrams}; (2) \textit{Data-Oriented Fishbone diagrams}; and, (3) \textit{Papin}.
\end{itemize}


\begin{center}\vspace{1cm}
\includegraphics[width=0.8\linewidth]{solution-overview}
\captionof{figure}{\color{tealblue} Solution Overview}
\end{center}

\color{DarkSlateGray} % SaddleBrown color for the conclusions to make them stand out

\section*{Data-Oriented Fishbone Models}

\begin{itemize}
\item \textit{Data-Oriented Fishbone models} link causes in a fishbone diagram with data in an object-oriented domain model by means of a so-called \textit{data feeder}.
\item Three kind of causes can be defined using this language: (1) \textit{compound causes}; (2) \textit{data-linked causes}; and, (3) \textit{not mapped causes}. 

\begin{center}\vspace{1cm}
	\includegraphics[width=0.8\linewidth]{dof}
	\captionof{figure}{\color{tealblue} An example of DOF specification and the \textit{ColumnSets} by means of the \textit{data feeders}}
\end{center}
\end{itemize}

\color{DarkSlateGray} % Set the color back to DarkSlateGray for the rest of the content

%----------------------------------------------------------------------------------------
%	FORTHCOMING RESEARCH
%----------------------------------------------------------------------------------------

\section*{Papin}

\begin{itemize}
	\item \textit{Papin} is used to specify which causes of a \textit{Data-Oriented Fishbone model} should be included in the result dataset as well as to create such dataset through an interpreter.
	\begin{center}\vspace{1cm}
		\includegraphics[width=0.45\linewidth]{papin}
		\captionof{figure}{\color{tealblue} An example of Papin specification}
	\end{center}
\end{itemize}


\section*{Evaluation}

\begin{itemize}
	\item Expresiveness of the DSLs developed in this project has been evaluated in five different case studies. No major problems were reported.
	\item Reductions in accidental complexity by 50\% and up to 80\% has shown that the developed DSLs perform better than their SQL and Pandas counterparts.

\end{itemize}


 %----------------------------------------------------------------------------------------
%	REFERENCES
%----------------------------------------------------------------------------------------

\nocite{*} % Print all references regardless of whether they were cited in the poster or not
\bibliographystyle{plain} % Plain referencing style
\bibliography{sample} % Use the example bibliography file sample.bib

%----------------------------------------------------------------------------------------
%	ACKNOWLEDGEMENTS
%----------------------------------------------------------------------------------------

\section*{Acknowledgements}

The project presented in this poster was developed in collaboration with Alfonso de la Vega, Diego García-Saiz and Pablo Sánchez, to whom's work is appreciated here.

%----------------------------------------------------------------------------------------

\end{multicols}
\end{document}