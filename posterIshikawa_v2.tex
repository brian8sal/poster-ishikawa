% !TeX spellcheck = en_US
\documentclass[25pt, a0paper, portrait, margin=0mm, innermargin=15mm,blockverticalspace=5mm, colspace=15mm, subcolspace=8mm]{tikzposter}

\input{packages}
\input{apparence}

\title{\parbox{1800pt}{Domain Specific Languages for the Automated Generation of Datasets for Industry 4.0}}

\author{Brian Sal, Diego García, Alfonso de la Vega, Pablo Sánchez}
\institute{Software Engineering and Real Time Group, Universidad de Cantabria, Spain}

\usetitlestyle[]{sampletitle}
\setlength{\columnseprule}{0.4pt}
\addbibresource{posterFishbone.bib}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\maketitle

% --- Lignes entre les colonnes
% Three columns
%\draw[DarkerGrey, line width=2mm, loosely dotted] (-13,43) -- (-13,-55); 
%\draw[DarkerGrey, line width=2mm, loosely dotted] (15,43) -- (15,-55);
%% Two cloumns 

\begin{columns}
\column{.33}

    %% HIGHLIGHTTS
    \block{\textsc{Highlights}}{
    	\begin{itemize}   
			%% TODO: Check these numbers are accurate
    		\item A set of \emph{Domain-Specific Languages (DSLs)} that reduces accidental complexity of dataset generation processes by 60\% and up to 90\%.
    		\item Data selection is performed over \emph{fishbone models}, a kind of model widely used in industrial contexts.   
%    		\item A first DSL links causes of a Ishikawa diagram with domain data. 
%			\item A second DSL generates datasets by selecting causes to be analyzed from Ishikawa diagram.
    	\end{itemize}
	}

	%% PROBLEM STATEMENT
    \block{\textsc{The Dataset Creation Problem}}{

		
		Most data mining algorithms only accept as input tabular data where all data related to each entity under analysis must be placed in a single row. As an example, Table 1 shows a dataset for analyzing defects in drive-half shafts manufactured in an assembly line. 
		
		\vspace{0.75cm}
		
		\parbox{700pt}{
		\hspace{0.65cm}
		\begin{small}
		\begin{tabular}{crrrrrrc}
			\hline
			\rowcolor{lightUC}
			\textcolor{white}{\textbf{ShaftId}} & 
			\textcolor{white}{\textbf{Hum.}}    & 
			\textcolor{white}{\textbf{Temp.}}   & 
			\textcolor{white}{\textbf{Press01}} & 
			\textcolor{white}{\textbf{Press02}} &
			\textcolor{white}{\textbf{Press03}} &
			\textcolor{white}{\textbf{Press04}} &
			\textcolor{white}{\textbf{IsOk}}
			 \\ \hline
			\rowcolor{LightGrey} 5209 & 56.7 & 20.4 & 7.9 & 7.9 & 7.8 & 7.9 & Y\\ 
			\rowcolor{white}     6556 & 58.0 & 19.8 & 7.2 & 7.1 & 7.1 & 7.1 & N \\ 
			\rowcolor{LightGrey} 7889 & 57.3 & 18.0 & 6.8 & 7.9 & 7.9 & 7.2 & N\\ 
		\end{tabular}
		\end{small}
		}
		\vspace{0.4cm}
		\begin{center}
			\textbf{Table 1.} A dataset for analyzing half-shafts defects.
		\end{center}
		
		\vspace{0.5cm}
	
		However, data available in a domain is not typically in that format. Domain entities often have complex relationships among them, such as depicted in Figure 1. In this case, to create the dataset of Table 1, we would need to navigate from the \texttt{DriveHalfShaft} class to the \texttt{AssemblySession}, \texttt{MonitoredParameters} and \texttt{PliersData} classes. In addition, we would have to place each pliers fastening in a separate column.
		
		\vspace{0.75cm}
		
		\innerblock{}{
			\centering \includegraphics[width=\linewidth]{domainModel.pdf}
		}
		\vspace{0.15cm}
		\begin{center}
			\textbf{Figure 1.} Domain model for half-shaft production.
		\end{center}

		\vspace{0.5cm}
		
		So, to create a dataset, data scientists need to write long and complex scripts that perform low-level operations, such as filters, joins or pivots, to transform domain data into an appropriate tabular representation. This is a time consuming and prone to error process. To alleviate this problem, we developed \textit{Lavoisier} in a previous work.
		
 	} %% The Dataset Generation Problem	
 
	\block{\textsc{Lavoisier}}{
		
		\textit{Lavoisier}~\cite{DelaVega2020} is a declarative language for dataset generation that provides a set of high-level primitives that allows data scientist to focus on what data must be selected and skip the details of how data need to be transformed. As an example, Figure 3 provides a Lavoisier script to generate the dataset of Table 1 from the domain model in Figure 1. Using Lavoisier, complexity of dataset generation processes decreases by 40\% and up to 80\%.
		
		\vspace{0.75cm}
		
		\innerblock{}{
			\lstinputlisting{listings/lavoisierScript.txt}
		}
		\vspace{0.25cm}
		\begin{center}
			\textbf{Figure 2.} Lavoisier script for the dataset of Table 1.
		\end{center}
	
		Lavoisier relies on object-oriented models to describe domain data, but industrial engineers are not used to deal with them. However, they are used to work with \emph{fishbone models}. So, we decided to adapt Lavoisier so that it worked with fishbone models. 
	
	}  %% Lavoisier

\column{.66}

%	\block{}{
%		\tikzstyle{na}=[baseline=-.25ex]
%		\tikzstyle{every picture}+=[remember picture]
%		\innerblock{Solution Overview}{
%			\centering \includegraphics[width=0.7\linewidth]{solutionOverview.pdf}
%		}
%		\vspace{0.5cm}
%		\centering \textbf{Figure 4.} Solution Overview.
%	} %% Solution Overview

	\begin{subcolumns}		
	\subcolumn{0.5}
		
		\block{\textsc{Fishbone Models}}{
			\emph{Fishbone Models}, also known as \emph{Ishikawa diagrams}, aim to identify causes that might lead to a certain effect. They are commonly used for quality control and process improvement in manufacturing settings~\cite{Tague2005}.
			\vspace{0.5cm}	
			\innerblock{}{
				%% TODO: Subir el tamaño de letra
				\centering \includegraphics[width=0.9\linewidth]{ishikawaSmall.pdf}
			}
			\vspace{0.5cm}
			\centering \textbf{Figure 3.} Ishikawa diagram for analyzing why half-shaft bands fall.
		}
	
		\block{\textsc{Solution}}{
			
			\innerblock{}{
				%% TODO: Quitar números
				%% TODO: Poner dataset debajo
				\centering \includegraphics[width=\linewidth]{solutionOverview.eps}
			}
			\vspace{0.35cm}
			\begin{center} 
				\textbf{Figure 4.} Solution overview.
			\end{center}
			
			\vspace{0.5cm}
			
			To make Lavoisier work with fishbone models, we created three DSLs~\cite{Sal2021}:
			\begin{description}
				\item[QCF (Quality-Control Fishbone Model)]: It is a classical fishbone model, as used in manucfacturing settings for quality control.
				\item[DOF (Data-Oriented Fishbone Model)]: It resembles a QCF linking causes with domain data. 
				\item[Papin]: It specifies what causes of a DOF should be included in a dataset. 
			\end{description}
		}	
	
		\block{\textsc{Data-Oriented Fishbones}}{
			
			\textit{Data-Oriented Fishbone (DOF) models} link elements in a fishbone diagram with data in a domain model. Three kinds of causes can be found in a DOF: 
			\begin{description}
				\item[Data-linked causes]: They contain special code blocks called data feeders (Figure 5, lines 7-9). A \emph{data-feeder} is composed of a Lavoisier statement that retrieves domain data to measure a cause. The result of evaluating this Lavoisier statement is a dataset for analyzing the associated cause.
				\item[Not mapped causes]: They are causes for which domain data is not available and they are used to keep traceability between QCFs and DOFs (Figure 4, lines 24-26). 
				\item[Compound causes]: They are comprised of other subcauses, which can be data-linked, not mapped or compound causes. They are evaluated by joining the datasets associated to its subcauses (Figure 4, lines 4-16).
			\end{description}
			
			Moreover, the effect is also linked to domain data using a \emph{data feeder}. 
			
		} %% DOFs	

	\subcolumn{0.5}
	
		\block{}{	
			\innerblock{}{
	
			 \lstinputlisting[style=dof]{listings/dof.txt}
			}
		
			\vspace{0.5cm}
			\centering \textbf{Figure 5.} DOF model.
			
		} %% DOF II
	
		\block{\textsc{Papin}}{
		
			\textit{Papin} selects causes of a DOF to be included in a dataset. Each cause is evaluated 
			and their output datasets are then joined. 
			
			\vspace{1cm}
			
			\innerblock{}{
				\lstinputlisting{listings/papinScript.txt}
			}
			\vspace{0.5cm}
			\centering \textbf{Figure 6.} Papin.
		} %% Papin
		
		\block{\textsc{Evaluation}}{
			\begin{itemize}
				\item Expressiveness of the DSLs has been evaluated with five industrial case studies. 
				\item Accidental complexity is reduced by 60\% and up to 90\% as compared to state-of-the-art technologies (e.g., SQL or Pandas).
				\item As compared to Lavoisier, this new approach adds an extra effort due to the need of specifying DOFs. When generating a small number of datasets from a same domain model, this extra complexity does not pay off, but, as this number grows, our approach starts to provide benefits.
			\end{itemize}
		} %% Evaluation
	
	   \block{}{
	   		\printbibliography
		}	

	\end{subcolumns}


        
      % --------------------------------- PARTIE 2  ----------    
%    \block{\textsc{Partie 2}}{
%        
%        \tikzstyle{na}=[baseline=-.25ex]
%        \tikzstyle{every picture}+=[remember picture]
%        \vspace{0.5cm}
%        
%        \innerblock{Avec titre}{\input{figures/figure1}
%        } 
%        \vspace{0.5cm}
%        \lipsum[1]
%            
%          \vspace{-2cm}}
          
    %%%%%%%%%%%%%%% COLONNE 2 %%%%%%%%%%%%%
    \subcolumn{.5}
    
        % ---------------------------------  PARTIE 3----------
%        \block{\textsc{Partie 3}}{\innerblock{}{
%        \input{figures/figure2}} \lipsum[2]
%        \vspace{-2cm}}
%            
%        \note[targetoffsetx = 3cm, targetoffsety = 1cm, angle = 20, connection]{\textbf{exemple de note}}
%
        
    % --------------------------------- PARTIE 4  ----------
%        \block{\textsc{Partie 4}}{ \lipsum[3]
%        \vspace{-2cm}}
    
  
 % ------------------------------------ PARTIE 5------------
%        \block{\textsc{Partie 5}}{\lipsum[1]  
%        
%        \input{table/table} 
%            \vspace{-2cm}}

 
 %%%%%%%%%%%%%%%%%%%%%%%% COLONNE 3 %%%%%%%%%%%%%%%%%%%%%%%%%   
%   \subcolumn{.33}

    % ---------------------------------------------
%        \block{\textsc{Conclusion}}{
%        \lipsum[1]
%        
%        \vspace{0.5cm}
%        \lipsum[2] \cite{ref1}
%        \vspace{0.5cm}}

    % ----------------------------------------------
%        \block{}{
%        \vspace{3cm}
%            \textbf{SPA}: Société Protectrice des Animaux $\bullet$
%            \textbf{CAF}: Caisse d'Allocations Familiales $\bullet$
%            \textbf{WER}: Word Error Rate }   
    

    % ---------------------------------  BIBLIO ----------
%    \block{}{
%    \vspace{1cm}
%	\printbibliography
%	}

\end{columns}

% ----------------- Ligne colorée à la fin du document -------------
\node [above right, text=white,outer sep=45pt,minimum width=\paperwidth, align=center, draw, fill=titledarkcolor, color=darkUC] at (-43.6,-61) { \textcolor{white}{\normalsize Contact: \{brian.sal, diego.garcia, alfonso.delavega, sanchezbp\}@unican.es}};

\end{document}
